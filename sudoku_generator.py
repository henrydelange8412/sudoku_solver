import random
import copy


class SudokuGenerator:

    @staticmethod
    def generate(difficulty):
        """generates a sudo random full board, and a sudoku board, with difficulty as the amount of empty slots"""
        size = 3
        num = random.sample(range(1, 10), len(range(1, 10)))
        full_board = []
        tuple_locations = []
        for i in range(9):
            temp = []
            for j in range(9):
                temp.append(num[SudokuGenerator.pattern(i, j, size)])
                tuple_locations.append((i, j))
            full_board.append(temp)
        board = copy.deepcopy(full_board)
        r = random.sample(tuple_locations, difficulty)
        for i in r:
            board[i[0]][i[1]] = 0
        return full_board, board

    @staticmethod
    def pattern(row_in, col_in, size):
        """creates a pattern for inserting the numbers into the sudoku that doesn't repeat (for 9 rows)
        insuring that there are no 2 numbers in the same row, column or box"""
        return (size * (row_in % size) + row_in // size + col_in) % (size*size)
