var hierarchy =
[
    [ "BackTracking", "class_back_tracking.html", null ],
    [ "IPointerEnterHandler", null, [
      [ "Slot", "class_slot.html", null ]
    ] ],
    [ "MonoBehaviour", null, [
      [ "CheckButton", "class_check_button.html", null ],
      [ "DrawSudoku", "class_draw_sudoku.html", null ],
      [ "HintButton", "class_hint_button.html", null ],
      [ "Slot", "class_slot.html", null ],
      [ "SolveButton", "class_solve_button.html", null ],
      [ "Sudoku", "class_sudoku.html", null ],
      [ "SudokuGenerator", "class_sudoku_generator.html", null ]
    ] ],
    [ "SlotDataPrefab", "class_slot_data_prefab.html", null ]
];