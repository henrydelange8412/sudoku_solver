var class_sudoku_generator =
[
    [ "Generate", "class_sudoku_generator.html#a8c9a9af71cd0065616b88e0f05a2f04a", null ],
    [ "Pattern", "class_sudoku_generator.html#ad383cf86eda844b53e631fdf0b568b55", null ],
    [ "backtracking_board", "class_sudoku_generator.html#a705b57f4277d29be17274431d75b7c32", null ],
    [ "board", "class_sudoku_generator.html#a1220ce6a1041d517e3c95b19627241fc", null ],
    [ "full_board", "class_sudoku_generator.html#af3964db6a5ac35e282b4520dbed6336f", null ],
    [ "num", "class_sudoku_generator.html#ad80b6b61f88a8df733baf2ebf76d0b7e", null ],
    [ "original_board", "class_sudoku_generator.html#a3f4a6d1b14dbd30faf021b7a5535f073", null ],
    [ "return_boards", "class_sudoku_generator.html#aee79fbcb0d0602cd5830efa0936b3fcb", null ],
    [ "size", "class_sudoku_generator.html#af014482c2efa1a2570e4faa946858ff1", null ],
    [ "tempum", "class_sudoku_generator.html#a07584b66a58208fdb696ce505e569af8", null ],
    [ "tuple_locations", "class_sudoku_generator.html#a16b7f1c0d180c549a0e2d38a5f1d8137", null ]
];