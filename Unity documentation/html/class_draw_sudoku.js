var class_draw_sudoku =
[
    [ "CheckSudokuButton", "class_draw_sudoku.html#a6316871f5f04656cca5e5356c9827ae9", null ],
    [ "DrawBoard", "class_draw_sudoku.html#a7d667be8606c515d6915455b750c595a", null ],
    [ "DrawNumbers", "class_draw_sudoku.html#abfa4897046f3637c920c39ac1b1c5ece", null ],
    [ "FullOrBackTracking", "class_draw_sudoku.html#a3bf670a088622c1ca787e45b3b9f8675", null ],
    [ "HintSudokuButton", "class_draw_sudoku.html#ab07522a1b65c32723e226cafbef20b13", null ],
    [ "InitBoard", "class_draw_sudoku.html#a3624c8bfcf6a4dff658dce917a639a91", null ],
    [ "InsertNumber", "class_draw_sudoku.html#a7e44f6a287be5be0ce69b7ef1730ac52", null ],
    [ "SetActiveHover", "class_draw_sudoku.html#aab25a6024a619ef576b151d805fa99da", null ],
    [ "SolveSudokuButton", "class_draw_sudoku.html#a94b6cfb8e6bf8aa40fdf029e6546567f", null ],
    [ "Update", "class_draw_sudoku.html#afd4b679a4f340c06a88c132219fddd17", null ],
    [ "BoardSlots", "class_draw_sudoku.html#ae3a7c9e732d9e62bc69f948c4c58715b", null ],
    [ "Canvas", "class_draw_sudoku.html#af20c33540ec978b92b9ad5e482447ce7", null ],
    [ "Generator", "class_draw_sudoku.html#a63eae7ecc53e8f7566ae1383e2e49655", null ],
    [ "key", "class_draw_sudoku.html#ad83529b0e2b42fb5760791c02f602e8d", null ],
    [ "SlotPrefab", "class_draw_sudoku.html#a7a6eb45461c135a72910bbbdb93fb3c1", null ],
    [ "x", "class_draw_sudoku.html#a53f529e51cc4631fdefb40f3dfb56932", null ],
    [ "y", "class_draw_sudoku.html#a5af1d12acf677b985dd47e9da0255382", null ]
];