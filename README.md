# Sudoku Solver

To start the python version, run the main.py file, you can change the difficulty by changing the variable "difficulty"

To start the Unity version, open the game_scene and press run, you can change the difficulty under the "Main Camera" GameObject with the variable "difficulty"