import pygame
import copy
import random
from backtracking import Backtracking


class GUI:

    def __init__(self, board, full_board):
        """initializes pygame, sets and creates the varies boards, sets the default settings for the game,
        sets the size of the pygame window, initializes the buttons and starts the game loop"""
        pygame.init()
        self.board = board
        self.full_board = full_board
        self.backtracking_board = Backtracking.return_board(copy.deepcopy(board))
        self.original_board = copy.deepcopy(board)
        self.temp_board = self.scratch_pad()
        self.font = pygame.font.SysFont("Arial", 35)
        self.background_color = (251, 247, 245)
        self.FPS = 60
        self.WIDTH = 550
        self.HEIGHT = 800
        self.win = pygame.display.set_mode((self.WIDTH, self.HEIGHT))
        self.win.fill(self.background_color)
        self.draw_frame()
        self.draw_board()
        self.hint_button = Button((200, 200, 200), 50, 515, 150, 70, "hint")
        self.solve_button = Button((200, 200, 200), 350, 515, 150, 70, "solve sudoku")
        self.check_button = Button((200, 200, 200), 50, 615, 150, 70, "check solution")
        print("Initializing complete, starting game...")
        self.game()

    def draw_frame(self):
        """draws the frame for the sudoku"""
        pygame.display.set_caption("Sudoku")
        for i in range(0, 10):
            if i % 3 == 0:
                pygame.draw.line(self.win, (0, 0, 0), (50 + 50 * i, 49), (50 + 50 * i, 502), 4)
                pygame.draw.line(self.win, (0, 0, 0), (49, 50 + 50 * i), (502, 50 + 50 * i), 4)
            pygame.draw.line(self.win, (0, 0, 0), (50 + 50 * i, 50), (50 + 50 * i, 500), 2)
            pygame.draw.line(self.win, (0, 0, 0), (50, 50 + 50 * i), (500, 50 + 50 * i), 2)
        return self.win

    def draw_board(self):
        """draws the numbers on the board of the sudoku"""
        for i in range(0, len(self.board[0])):
            for j in range(0, len(self.board[0])):
                if 0 < self.board[i][j] < 10:
                    value = self.font.render(str(self.board[i][j]), True, (52, 31, 151))
                    self.win.blit(value, (j * 50 + 67, i * 50 + 55))
        return self.win

    def scratch_pad(self):
        """creates 4 temp slots for each location on the board"""
        scratch = []
        for i in range(len(self.board)):
            temp = []
            for j in range(len(self.board)):
                temp.append([0, 0, 0, 0])
            scratch.append(temp)
        return scratch

    def insert(self, position_in, button_in, key_in):
        """inserts a number into the sudoku.
        position_in is the location of the mouse at the moment of pressing a key.
        button_in is the insert method, those being 1 for a normal number, or 3 for a temp number
        these being the left and right mouse buttons.
        key_in is the number that was pressed.
        when 0 is pressed, the input location is cleared.
        """
        i, j = position_in[1], position_in[0]
        if button_in == 3:
            font = pygame.font.SysFont("Arial", 20)
        else:
            font = self.font
        value = font.render(str(key_in - 48), True, (0, 0, 0))
        if self.original_board[i - 1][j - 1] != 0:
            return
        """0 is pressed"""
        if key_in == 48:
            self.board[i - 1][j - 1] = key_in - 48
            self.temp_board[i - 1][j - 1] = [0, 0, 0, 0]
            pygame.draw.rect(self.win, self.background_color, (position_in[0] * 50 + 2.5, position_in[1] * 50 + 2.5, 49, 49))
            pygame.display.update()
            return
        """the insert mode is temp and the key input is between 1 and 9"""
        if button_in == 3 and 0 < key_in - 48 < 10:
            if self.temp_board[i - 1][j - 1][0] == 0:
                self.temp_board[i - 1][j - 1][0] = key_in - 48
                self.win.blit(value, (position_in[0] * 50 + 5, position_in[1] * 50 + 1))
                return
            elif self.temp_board[i - 1][j - 1][1] == 0:
                self.temp_board[i - 1][j - 1][1] = key_in - 48
                self.win.blit(value, (position_in[0] * 50 + 5, position_in[1] * 50 + 25))
                return
            elif self.temp_board[i - 1][j - 1][2] == 0:
                self.temp_board[i - 1][j - 1][2] = key_in - 48
                self.win.blit(value, (position_in[0] * 50 + 37, position_in[1] * 50 + 1))
                return
            else:
                pygame.draw.rect(self.win, self.background_color, (position_in[0] * 50 + 35, position_in[1] * 50 + 25, 12, 21))
                self.temp_board[i - 1][j - 1][3] = key_in - 48
                self.win.blit(value, (position_in[0] * 50 + 37, position_in[1] * 50 + 25))
                return
        """the insert mode is normal and the key input is between 1 and 9"""
        if 0 < key_in - 48 < 10:
            self.temp_board[i - 1][j - 1] = [0, 0, 0, 0]
            pygame.draw.rect(self.win, self.background_color,(position_in[0] * 50 + 2.5, position_in[1] * 50 + 2.5, 49, 49))
            self.win.blit(value, (position_in[0] * 50 + 17, position_in[1] * 50 + 5))
            self.board[i - 1][j - 1] = key_in - 48
            return

    def redraw_window(self):
        """draws the buttons and their border."""
        self.hint_button.draw(self.win, (0, 0, 0))
        self.solve_button.draw(self.win, (0, 0, 0))
        self.check_button.draw(self.win, (0, 0, 0))
        return

    def victory_window(self):
        """creates a victory window"""
        pygame.display.set_caption("You won!!")
        self.win.fill(self.background_color)
        self.win.blit(pygame.font.SysFont("Arial", 100).render("You won!!", True, (25, 25, 25)), (100, 300))
        return

    def button_functions(self, int_in):
        """passes the button presses from the main game-loop to the right functions"""
        if int_in == 0:
            self.give_hint()
            return
        if int_in == 1:
            self.solve_sudoku()
            return
        if int_in == 2:
            if self.validate():
                return True

    def test_full_or_backtracking(self):
        """tests if the full board, or the backtracking board have more correct numbers"""
        test1 = 0
        test2 = 0
        for i in range(len(self.board)):
            for j in range(len(self.board)):
                if self.board[i][j] == self.full_board[i][j]:
                    test1 += 1
                if self.board[i][j] == self.backtracking_board[i][j]:
                    test2 += 1
        return test1, test2

    def give_hint(self):
        """checking with the test_full_or_backtracking function,
        gives a hint for the board with the most correct inputs"""
        test1, test2 = self.test_full_or_backtracking()
        tuple_locations = []
        for i in range(len(self.board)):
            for j in range(len(self.board)):
                if self.board[i][j] == 0:
                    tuple_locations.append((i, j))
        if len(tuple_locations) < 1:
            return
        hint = random.sample(tuple_locations, 1).pop()
        pygame.draw.rect(self.win, self.background_color, (hint[1] * 50 + 52.5, hint[0] * 50 + 52.5, 45, 45))
        if test1 >= test2:
            num = self.full_board[hint[0]][hint[1]]
        else:
            num = self.backtracking_board[hint[0]][hint[1]]
        value = self.font.render(str(num), True, (100, 31, 52))
        self.win.blit(value, (hint[1] * 50 + 67, hint[0] * 50 + 55))
        self.board[hint[0]][hint[1]] = num
        self.draw_frame()
        return

    def solve_sudoku(self):
        """checking with the test_full_or_backtracking function, which board is closest to completion
        and fills in the rest of the board according to the best solution."""
        test1, test2 = self.test_full_or_backtracking()
        if test1 >= test2:
            self.board = copy.deepcopy(self.full_board)
            print(f"\nThe sudoku was solved using the original board.")
        else:
            self.board = copy.deepcopy(self.backtracking_board)
            print(f"\nThe sudoku was solved using the backtracking board.")
        for i in range(len(self.board)):
            for j in range(len(self.board)):
                self.temp_board[i - 1][j - 1] = [0, 0, 0, 0]
                if self.original_board[i][j] == 0:
                    num = self.board[i][j]
                    value = self.font.render(str(num), True, (0, 0, 0))
                    pygame.draw.rect(self.win, self.background_color, (j * 50 + 52.5, i * 50 + 52.5, 49, 49))
                    self.win.blit(value, (j * 50 + 67, i * 50 + 55))
        self.draw_frame()
        return

    def validate(self):
        """first checks which board has the most correct inputs with the test_full_or_backtracking method,
        checks all user input numbers comparing them to the chosen board.
        if all the inputs are correct, returns True"""
        test1, test2 = self.test_full_or_backtracking()
        if test1 == len(self.board)*len(self.board) or test2 == len(self.board)*len(self.board):
            print("\nNice, you won!!")
            return True
        print(f"\nCompared to the original board there are {test1} correct inputs.")
        print(f"Compared to the backtracking board there are {test2} correct inputs.")
        for i in range(len(self.board)):
            for j in range(len(self.board)):
                num = self.board[i][j]
                value = self.font.render(str(num), True, (0, 0, 0))
                if test1 >= test2:
                    self.temp_board[i][j] = [0, 0, 0, 0]
                    if self.board[i][j] == self.full_board[i][j] and self.original_board[i][j] == 0 and self.board[i][j] != 0:
                        pygame.draw.rect(self.win, (0, 255, 0), (j * 50 + 52.5, i * 50 + 52.5, 49, 49))
                        self.win.blit(value, (j * 50 + 67, i * 50 + 55))
                    if self.board[i][j] != self.full_board[i][j] and self.original_board[i][j] == 0 and self.board[i][j] != 0:
                        pygame.draw.rect(self.win, (255, 0, 0), (j * 50 + 52.5, i * 50 + 52.5, 49, 49))
                        self.win.blit(value, (j * 50 + 67, i * 50 + 55))
                if test1 < test2:
                    self.temp_board[i][j] = [0, 0, 0, 0]
                    if self.board[i][j] == self.backtracking_board[i][j] and self.original_board[i][j] == 0 and self.board[i][j] != 0:
                        pygame.draw.rect(self.win, (0, 255, 0), (j * 50 + 52.5, i * 50 + 52.5, 49, 49))
                        self.win.blit(value, (j * 50 + 67, i * 50 + 55))
                    if self.board[i][j] != self.backtracking_board[i][j] and self.original_board[i][j] == 0 and self.board[i][j] != 0:
                        pygame.draw.rect(self.win, (255, 0, 0), (j * 50 + 52.5, i * 50 + 52.5, 49, 49))
                        self.win.blit(value, (j * 50 + 67, i * 50 + 55))
        self.draw_frame()
        return False

    def game(self):
        """the main game loop"""
        clock = pygame.time.Clock()
        run = True
        key = 1
        while run:
            self.redraw_window()
            pygame.display.update()
            clock.tick(self.FPS)
            for event in pygame.event.get():
                pos = pygame.mouse.get_pos()
                if event.type == pygame.MOUSEMOTION:
                    """changes the color of the buttons when hovering over them."""
                    if self.hint_button.is_over(pos):
                        self.hint_button.color = (220, 220, 220)
                    else:
                        self.hint_button.color = (200, 200, 200)
                    if self.solve_button.is_over(pos):
                        self.solve_button.color = (220, 220, 220)
                    else:
                        self.solve_button.color = (200, 200, 200)
                    if self.check_button.is_over(pos):
                        self.check_button.color = (220, 220, 220)
                    else:
                        self.check_button.color = (200, 200, 200)
                if event.type == pygame.MOUSEBUTTONDOWN:
                    """sets the insert mode for numbers with the "key" variable
                    and calls the associated functions when pressing the buttons"""
                    key = event.button
                    if self.hint_button.is_over(pos):
                        self.button_functions(0)
                    if self.solve_button.is_over(pos):
                        self.button_functions(1)
                    if self.check_button.is_over(pos):
                        """if validate returns True, end the game loop and call the victory window"""
                        if self.button_functions(2):
                            run = False
                if event.type == pygame.KEYDOWN:
                    """checks if the mouse is on the board and inserts a number at that spot with the insert function"""
                    if 50 < pos[0] < 500 and 50 < pos[1] < 500:
                        GUI.insert(self, (pos[0] // 50, pos[1] // 50), key, event.key)
                        GUI.draw_frame(self)
                        pygame.display.update()
                if event.type == pygame.QUIT:
                    run = False
                    pygame.quit()
                    quit()
        run = True
        self.victory_window()
        while run:
            pygame.display.update()
            clock.tick(self.FPS)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                    pygame.quit()
                    quit()

class Button:

    def __init__(self, color, x, y, width, height, text=''):
        """creates a standard Button object"""
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text

    def draw(self, win, outline=None):
        """draws the shape and basic details of the button"""
        if outline:
            pygame.draw.rect(win, outline, (self.x - 3, self.y - 3, self.width + 6, self.height + 6), 0)
        pygame.draw.rect(win, self.color, (self.x, self.y, self.width, self.height), 0)
        if self.text != '':
            font = pygame.font.SysFont("Arial", 20)
            text = font.render(self.text, True, (0, 0, 0))
            win.blit(text, (
                self.x + (self.width / 2 - text.get_width() / 2), self.y + (self.height / 2 - text.get_height() / 2)))

    def is_over(self, pos):
        """checks if the mouse is over the button"""
        if self.x < pos[0] < self.x + self.width:
            if self.y < pos[1] < self.y + self.height:
                return True

        return False

