from sudoku_generator import SudokuGenerator
from interface import GUI


class Sudoku:

    def __init__(self):
        """creates a Sudoku object, difficulty is the amount of empty spots in the sudoku"""
        self.difficulty = 35
        self.full_board, self.board = self.create_board()
        self.open_GUI()

    def create_board(self):
        """generates the original and the game Sudoku board"""
        if self.difficulty < 1 or self.difficulty > 81:
            self.difficulty = 35
        instance = SudokuGenerator.generate(self.difficulty)
        """prints the sudoku in the console """
        self.print_board(instance[0])
        self.print_board(instance[1])
        return instance

    @staticmethod
    def print_board(board_in):
        """makes a print-out of the input board in the console"""
        for i in range(len(board_in)):
            if i % 3 == 0:
                print("+ - - - + - - - + - - - +")
            for j in range(len(board_in)):
                if j % 3 == 0:
                    print("| ", end="")
                if j == (len(board_in)-1):
                    print(board_in[i][j], "| ")
                else:
                    print(str(board_in[i][j]) + " ", end="")
            if i == (len(board_in)-1):
                print("+ - - - + - - - + - - - +")
        print("")
        return


    def open_GUI(self):
        """Opens the pygame UI"""
        return GUI(self.board, self.full_board)


sudoku = Sudoku()


