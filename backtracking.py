class Backtracking:

    def __init__(self, board):
        """calls the backtracking algorithm"""
        self.board = board
        self.return_board(board)


    @staticmethod
    def return_board(board):
        """returns the board from backtracking"""
        Backtracking.solve(board)
        return board


    @staticmethod
    def solve(board):
        """looks for an empty slot and inserts a random number into it, calling validate to check if
        the random number is allowed if it is, call solve recursively until all the slots are solved."""
        find = Backtracking.find_empty(board)
        if not find:
            return True
        else:
            row, col = find
        for i in range(1, 10):
            if Backtracking.validate(board, i, (row, col)):
                board[row][col] = i
                if Backtracking.solve(board):
                    return True
                board[row][col] = 0
        return False



    @staticmethod
    def find_empty(board):
        """finds an empty slot on the sudoku"""
        for i in range(len(board)):
            for j in range(len(board[0])):
                if board[i][j] == 0:
                    return i, j  # row, col
        return None


    @staticmethod
    def validate(board, num, pos):
        """validates the inserted numbers by checking the rows cols and the 3x3 box"""
        valid = True
        for i in range(len(board)):
            if board[pos[0]][i] == num and pos[1] != i:
                return False

        for i in range(len(board)):
            if board[i][pos[1]] == num and pos[0] != i:
                return False

        row_section = pos[1] // 3
        col_section = pos[0] // 3
        for i in range(col_section * 3, col_section * 3 + 3):
            for j in range(row_section * 3, row_section * 3 + 3):
                if board[i][j] == num and (i, j) != pos:
                    return False
        return valid
