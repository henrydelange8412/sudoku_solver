using UnityEngine;
using UnityEngine.UI;

/// creates a hint button.
///
/// creates a hint button and calls the HintSudokuButton function from the DrawSudoku class.
public class HintButton : MonoBehaviour
{
    /// creates Hint a slot for the Button-Object.
    public Button Hint;
    /// creates Draw a slot for the DrawSudoku-Object.
    public DrawSudoku Draw;

    void Start()
    {
        /// checks if the user presses the button.
        Button btn = Hint.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        /// calls the HintSudokuButton function.
        Draw.HintSudokuButton();
        print("Hint clicked");
    }
}