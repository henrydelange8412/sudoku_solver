using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// creates the board slots.
///
/// creates slots to store x y and number data in during the game.
public class Slot : MonoBehaviour, IPointerEnterHandler 
{ 
    /// creates 2 ints MyOwnX and MyOwnY.
    public int MyOwnX, MyOwnY;
    /// creates a slot for the MainSlot Text-Object.
    public Text MainSlot;
    /// creates a list of slots for the TempSlots Text-Objects.
    public List<Text> TempSlots;

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        /// checks if the mouse enters and is over a Slot and returns the x and y location of the mouse at that position.
        GetComponentInParent<DrawSudoku>().SetActiveHover(MyOwnX, MyOwnY);
    }
}
