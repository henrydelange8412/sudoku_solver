using UnityEngine;

/// backtracking algorithm.
///
/// implements the backtracking algorithm, when called uses it on the input board.
public static class BackTracking
{
    public static int[][] return_board(int[][] board)
    {
        /// takes the input board and preforms a backtracking algorithm on it and returns the solution.
        /// 
        /// calls the backtracking algorithm with the solve function.
        BackTracking.solve(board);
        return board;
    }

    public static bool solve(int[][] board)
    {
        /// takes the input board and searches for empty slots, tries to input the lowest number.
        /// 
        /// if the lowest number is allowed, continues to the next empty slot, if all numbers have been tried for a single slot,
        /// 
        /// and not a single one was allowed, it removes the number in the previous slot and tries if a different number is allowed in that spot.
        Vector3Int find = BackTracking.find_empty(board);
        int row;
        int col;
        if (find[2] == 1)
        {
            return true;
        }
        else
        {
            row = find[0];
            col = find[1];
        }
        Vector2Int pos = new Vector2Int(row, col);
        for (int i = 1; i < 10; i++)
        {
            if (BackTracking.validate(board, i, pos))
            {
                board[row][col] = i;
                if (BackTracking.solve(board))
                {
                    return true;
                }
                board[row][col] = 0;
            }
        }
        return false;
    }

    public static Vector3Int find_empty(int[][] board)
    {
        /// loops trough the board and checks if there is an empty slot.
        /// 
        /// returns a vector3Int with the x and y location of the slot, and a boolean value,
        /// 
        /// when all slots are filled returns a vector3Int with 0, 0, 1.
        for(int i = 0; i < 9; i++)
        {
            for(int j = 0; j < 9; j++)
            {
                if(board[i][j] == 0)
                {
                    return new Vector3Int(i, j, 0);
                }
            }
        }
        return new Vector3Int(0, 0, 1);
    }

    public static bool validate(int[][] board, int num, Vector2Int pos)
    {
        /// validates the Sudoku at every input.
        /// 
        /// checking if the number that was input is allowed in the current spot, by comparing the row, column and box.
        bool valid = true;
        for (int i = 0; i < 9; i++)
        {
            if (board[pos[0]][i] == num && pos[1] != i)
            {
                return false;
            }
        }
        for (int i = 0; i < 9; i++)
        {
            if (board[i][pos[1]] == num && pos[0] != i)
            {
                return false;
            }
        }
        int row_section = pos[1] / 3;
        int col_section = pos[0] / 3;
        for (int i = col_section*3; i < (col_section * 3 + 3); i++)
        {
            for (int j = row_section * 3; j < (row_section * 3 + 3); j++)
            {
                if (board[i][j] == num && new Vector2Int(i, j) != pos)
                {
                    return false;
                }
            }
        }
        return valid;
    }
}
