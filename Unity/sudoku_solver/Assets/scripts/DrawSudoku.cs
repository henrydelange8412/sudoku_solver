using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// Draws the sudoku.
///
/// DrawSudoku class handles everything related to drawing the GUI.
/// 
/// also provides the functions for the buttons.
public class DrawSudoku : MonoBehaviour
{
    /// creates x int variable.
    private int x;
    /// creates y int variable.
    private int y;
    /// creates key int variable.
    private int key;
    /// creates SlotPrefab a slot for the GameObject.
    public GameObject SlotPrefab;
    /// creates boardSlots an array of 9x9 for the Slot_Object.
    public Slot[,] BoardSlots = new Slot[9,9];
    /// creates Generator a slot for the SudokuGenerator-Object.
    public SudokuGenerator Generator;
    /// creates Canvas a slot for the GameObject.
    public GameObject Canvas;
    
    public Slot[,] InitBoard() 
    {
        /// initializes a 9x9 grid of Slot objects.
        for (int i = 0; i < 9; i++) 
        {
            for (int j = 0; j < 9; j++) 
            {
                BoardSlots[i, j] = Instantiate(SlotPrefab, transform).GetComponent<Slot>();
                BoardSlots[i, j].MyOwnX = i;
                BoardSlots[i, j].MyOwnY = j;
            }
        }
        return BoardSlots;
    }
    
    public void DrawBoard(int[][][] return_boards)
    {
        /// calls the DrawNumbers function for every item in the 9x9 array.
        for(int i = 0; i < 9; i++)
        {
            for(int j = 0; j < 9; j++)
            {
                DrawNumbers(i, j, return_boards[1][i][j]);
            }
        }
    }

    private void Update() 
    {
        /// checks if the user is switching input modes with key presses, also checks if the user is pressing a key.
        for (int i = 0; i <= 9; i++) 
        {
            if (Input.GetMouseButtonDown(0))
            {
                key = 0;
            }
            if(Input.GetMouseButtonDown(1))
            {
                key = 1;
            }
            if (Input.GetKeyDown(i.ToString()))
            {
                InsertNumber(x, y, i, key);
            }
        }
    }

    public void DrawNumbers(int x, int y, int number)
    {
        /// Draws the number input on the location of x and y on the board also clears the TempSlots.
        for (int i = 0; i <4; i++)
            {
                BoardSlots[x, y].TempSlots[i].text = "";
            }
        if (number == 0)
        {
            BoardSlots[x, y].MainSlot.text = null;
        }
        if(0 < number && number < 10)
        {
            BoardSlots[x, y].MainSlot.text = number.ToString();
            Generator.return_boards[1][x][y] = number;
        }
    }

    public void InsertNumber(int x, int y, int number, int key) 
    {   
        /// Draws the number input on the location of x and y on the board using key as the input method.
        /// 
        /// also checks if the input is valid and not in a location where the original_board has a number.
        /// 
        /// if the input is 0, clears the MainSlot and the TempSlots.
        if (Generator.return_boards[2][x][y] != 0)
        {
            return;
        }
        if (number == 0)
        {
            BoardSlots[x, y].MainSlot.text = null;
            for (int i = 0; i <4; i++)
            {
                BoardSlots[x, y].TempSlots[i].text = "";
            }
            Generator.return_boards[1][x][y] = number;
        }
        if(0 < number && number < 10)
        {
            if(key == 0)
            {
                BoardSlots[x, y].MainSlot.text = number.ToString();
                Generator.return_boards[1][x][y] = number;
                for (int i = 0; i <4; i++)
                {
                    BoardSlots[x, y].TempSlots[i].text = "";
                }
            }
            else
            {
                if (BoardSlots[x, y].TempSlots[0].text == "")
                {
                    BoardSlots[x, y].TempSlots[0].text = number.ToString();
                }
                else if (BoardSlots[x, y].TempSlots[1].text == "")
                {
                    BoardSlots[x, y].TempSlots[1].text = number.ToString();
                }
                else if (BoardSlots[x, y].TempSlots[2].text == "")
                {
                    BoardSlots[x, y].TempSlots[2].text = number.ToString();
                }
                else
                {
                    BoardSlots[x, y].TempSlots[3].text = number.ToString();
                }
            }
        }
    }

    public void SetActiveHover(int _x, int _y) 
    {
        /// sets the active hover location to that of the mouse location.
        x = _x;
        y = _y;
    }

    public Vector2Int FullOrBackTracking()
    {
        /// checks how many numbers in the full_board are correct and compares them to the backtracking board.
        /// 
        /// returns a Vector2Int with the total correct numbers in each board.
        int test1 = 0;
        int test2 = 0;
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                if (Generator.return_boards[1][i][j] == Generator.return_boards[0][i][j])
                {
                    test1 +=1;
                }
                if (Generator.return_boards[1][i][j] == Generator.return_boards[3][i][j])
                {
                    test2 +=1;
                }
            } 
        }
        Vector2Int return_tests = new Vector2Int(test1, test2);
        return return_tests;
    }

    public void SolveSudokuButton()
    {
        /// gets called on pressing the SolveButton.
        /// 
        /// checks which solution is closer to being complete and solves the sudoku with that solution.
        Vector2Int tests = FullOrBackTracking();
        int test1 = tests[0];
        int test2 = tests[1];
        int board;
        if (test1 >= test2)
        {
            board = 0;
            print("The sudoku was solved using the original board.");
        }
        else
        {
            board = 3;
            print("The sudoku was solved using the backtracking board.");
        }
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                for (int k = 0; k <4; k++)
                {
                    BoardSlots[x, y].TempSlots[k].text = "";
                }
                if (Generator.return_boards[2][i][j] == 0)
                {
                    int num = Generator.return_boards[board][i][j];
                    DrawNumbers(i, j, num);
                }
            }
        } 
    }

    public void HintSudokuButton()
    {
        /// gets called on pressing the HintButton.
        /// 
        /// checks which solution is closer to being complete and gives a hint for the board that's closest to being complete.
        Vector2Int tests = FullOrBackTracking();
        int test1 = tests[0];
        int test2 = tests[1];
        int board;
        List<Vector2Int> tuple_locations = new List<Vector2Int>{};
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
                if (Generator.return_boards[1][i][j] == 0)
                {
                    tuple_locations.Add(new Vector2Int(i, j));
                }
            }
        }    
        if (tuple_locations.Count < 1)
        {
            return;
        }
        if (test1 >= test2)
        {
            board = 0;
            print("Using the original board.");
        }
        else
        {
            board = 3;
            print("Using the backtracking board.");
        }
        int index = Random.Range(0, tuple_locations.Count);
        Vector2Int location = tuple_locations[index];
        int num = Generator.return_boards[board][location[0]][location[1]];
        DrawNumbers(location[0], location[1], num);
        tuple_locations.RemoveAt(index); 
    }

    public void CheckSudokuButton()
    {
        /// gets called on pressing the CheckButton.
        /// 
        /// checks which solution is closer to being complete and uses that board that's closed to being complete.
        /// 
        /// loops trough all the slots and colors a slot green if it's correct and red if it's incorrect.
        /// 
        /// if the board is finished with either solution, shows the VictoryScreen.
        Vector2Int tests = FullOrBackTracking();
        int test1 = tests[0];
        int test2 = tests[1];
        int board;
        print("click test");
        if (test1 == 81 || test2 == 81)
        {
            Canvas.SetActive(true);
            return; // you won
        }
        if (test1 >= test2)
        {
            board = 0;
            print("Using the original board.");
        }
        else
        {
            board = 3;
            print("Using the backtracking board.");
        }
        Debug.Log($"Compared to the original board there are {test1} correct inputs.");
        Debug.Log($"Compared to the backtracking board there are {test2} correct inputs.");
        for (int i = 0; i < 9; i++)
        {
            for (int j = 0; j < 9; j++)
            {
               int num = Generator.return_boards[board][i][j];
               if (Generator.return_boards[1][i][j] == Generator.return_boards[board][i][j] && Generator.return_boards[2][i][j] == 0 && Generator.return_boards[1][i][j] != 0)
               {
                BoardSlots[i, j].GetComponent<Image>().color = new Color(0.2f, 0.9f, 0.2f);
                for (int k = 0; k <4; k++)
                {
                    BoardSlots[i, j].TempSlots[k].text = "";
                }
               }
               if (Generator.return_boards[1][i][j] != Generator.return_boards[board][i][j] && Generator.return_boards[2][i][j] == 0 && Generator.return_boards[1][i][j] != 0)
               {
                BoardSlots[i, j].GetComponent<Image>().color = new Color(0.9f, 0.2f, 0.2f);
               }
            }
        }
    }
}
