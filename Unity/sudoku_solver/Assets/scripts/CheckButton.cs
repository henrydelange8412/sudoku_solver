using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// creates a check button.
///
/// creates a check button and calls the CheckSudokuButton function from the DrawSudoku class.
public class CheckButton : MonoBehaviour
{
    /// creates Check a slot for the Button-Object.
    
    public Button Check;
    /// creates Draw a slot for the DrawSudoku-Object.
    public DrawSudoku Draw;

    void Start()
    {
        /// checks if the user presses the button.
        Button btn = Check.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        /// calls the CheckSudokuButton function.
        Draw.CheckSudokuButton();
        print("Check clicked");
    }
}