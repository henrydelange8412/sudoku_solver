using System.Collections.Generic;
using UnityEngine;

/// generates the sudoku.
///
/// generates the 4 boards used to solve the sudoku.
public class SudokuGenerator : MonoBehaviour
{
    /// creates size a variable of int 3.
    private int size = 3;
    /// creates tempum a list of ints between 1 and 9.
    private List<int> tempum = new List<int>{1, 2, 3, 4, 5, 6, 7, 8, 9};
    /// creates num an empty array with 9 slots.
    private int[] num = new int[9];
    /// creates full_board a 2D array with 9 slots.
    private int[][] full_board = new int[9][];
    /// creates board a 2D array with 9 slots.
    private int[][] board = new int[9][];
    /// creates original_board a 2D array with 9 slots.
    private int[][] original_board = new int[9][];
    /// creates backtracking_board a 2D array with 9 slots.
    private int[][] backtracking_board = new int[9][];
    /// creates return_boards an array of 2D array with 4 slots.
    public int[][][] return_boards = new int[4][][];
    /// creates tuple_locations an empty list of vector2ints.
    private List<Vector2Int> tuple_locations = new List<Vector2Int>{};

    public int[][][] Generate(int difficulty) 
    {

        /// Generates the sudoku with difficulty as the amount of empty slots in the sudoku.
        /// 
        /// also generates all 4 different boards.
        /// 
        /// full_board is the fully generated board.
        /// 
        /// board is the full_board with numbers removed, the user will play on this board.
        /// 
        /// original_board, is also the full_board with numbers removed, but stored separately so it can be used in comparisons later.
        /// 
        /// backtracking_board is a second solution to the board using the backtracking algorithm.
        /// 
        /// this function returns return_boards which is an array of all 4 boards.
        for(int i = 0; i < 9; i++)
        {
            int index = Random.Range(0, tempum.Count);
            int number = tempum[index];
            num[i] = tempum[index];
            tempum.RemoveAt(index);       
        }
        for(int i = 0; i < 9; i++)
        {
            int[] temp1 = new int[9];
            int[] temp2 = new int[9];
            int[] temp3 = new int[9];
            int[] temp4 = new int[9];
            for(int j = 0; j < 9; j++)
            {
                temp1[j] = num[Pattern(i, j, size)];
                temp2[j] = num[Pattern(i, j, size)];
                temp3[j] = num[Pattern(i, j, size)];
                temp4[j] = num[Pattern(i, j, size)];
                tuple_locations.Add(new Vector2Int(i, j));
            }
            full_board[i] = temp1;
            board[i] = temp2;
            original_board[i] = temp3;
            backtracking_board[i] = temp4;
        }
        for(int i = 0; i < difficulty; i++)
        {
            int index = Random.Range(0, tuple_locations.Count);
            Vector2Int location = tuple_locations[index];
            board[location[0]][location[1]] = 0;
            original_board[location[0]][location[1]] = 0;
            backtracking_board[location[0]][location[1]] = 0;
            tuple_locations.RemoveAt(index);
        }
        BackTracking.return_board(backtracking_board);
        return_boards[0] = full_board;
        return_boards[1] = board;
        return_boards[2] = original_board;
        return_boards[3] = backtracking_board;
        return return_boards;
    }

    static int Pattern(int row_in, int col_in, int size)
    {
        /// creates a pattern for inserting the numbers into the sudoku that doesn't repeat (for 9 rows).
        /// 
        /// insuring that there are no 2 numbers in the same row, column or box.
        return (size * (row_in % size) + row_in / size + col_in) % (size*size);
    }
}