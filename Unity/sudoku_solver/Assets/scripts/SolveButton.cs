using UnityEngine;
using UnityEngine.UI;

/// creates a solve button.
///
/// creates a solve button and calls the SolveSudokuButton function from the DrawSudoku class.
public class SolveButton : MonoBehaviour
{
    /// creates Solve a slot for the Button-Object.
    public Button Solve;
    /// creates Draw a slot for the DrawSudoku-Object.
    public DrawSudoku Draw;

    void Start()
    {
        /// checks if the user presses the button.
        Button btn = Solve.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        /// calls the SolveSudokuButton function.
        Draw.SolveSudokuButton();
        print("Solve clicked");
    }
}
