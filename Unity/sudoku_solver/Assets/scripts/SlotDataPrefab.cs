using UnityEngine;

/// creates a SlotDataPrefab.
///
/// creates a SlotDataPrefab and initializes it.
public class SlotDataPrefab
{
    /// creates _row int variable.
    private int _row;
    /// creates _col int variable.
    private int _col;
    /// creates _instance a slot for the GameObject.
    private GameObject _instance;
    /// gets and sets Row to _row.
    public int Row {get => _row; set => _row = value;}
    /// gets and sets Col to _col.
    public int Col {get => _col; set => _col = value;}


    public SlotDataPrefab(GameObject instance_in, int row_in, int col_in)
    {
        /// instantiates SlotDataPrefab
        _instance = instance_in;
        _row = row_in;
        _col = col_in;
    }
}