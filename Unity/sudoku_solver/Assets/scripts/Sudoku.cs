using UnityEngine;

/// initializes a Sudoku object.
///
/// Creates an initializes a Sudoku object, and calls the functions to start the game.
public class Sudoku : MonoBehaviour
{
    /// creates difficulty int variable.
    public int difficulty;
    /// creates return_boards an array with 2D arrays.
    public int[][][] return_boards;
    /// creates DrawSudokuPanel a slot for the DrawSudokuPanel-Object.
    public DrawSudoku DrawSudokuPanel;
    /// creates Generator a slot for the SudokuGenerator-Object.
    public SudokuGenerator Generator;

    private void Start() 
    {
        /// calls the CreateBoard function.
        /// 
        /// calls the InitBoard function.
        /// 
        /// calls the DrawBoard function with return_boards.
        return_boards = CreateBoard();
        DrawSudokuPanel.InitBoard();
        DrawSudokuPanel.DrawBoard(return_boards);
    }

    private int[][][] CreateBoard() 
    {
        /// checks if the difficulty input is valid, if not, set it to 35.
        /// 
        /// calls the Generate function with difficulty to generate all 4 boards for the sudoku.
        /// 
        /// returns the 4 generated boards.
        if (difficulty < 1 || difficulty > 81)
            difficulty = 35;
        int[][][] return_boards = Generator.Generate(difficulty);
        return return_boards;
    }
}
