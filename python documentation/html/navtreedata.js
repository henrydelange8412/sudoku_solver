/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Sudoku_solver", "index.html", [
    [ "Sudoku Solver", "md__r_e_a_d_m_e.html", [
      [ "Getting started", "md__r_e_a_d_m_e.html#autotoc_md1", null ],
      [ "Add your files", "md__r_e_a_d_m_e.html#autotoc_md2", null ],
      [ "Integrate with your tools", "md__r_e_a_d_m_e.html#autotoc_md3", null ],
      [ "Collaborate with your team", "md__r_e_a_d_m_e.html#autotoc_md4", null ],
      [ "Test and Deploy", "md__r_e_a_d_m_e.html#autotoc_md5", null ],
      [ "Editing this README", "md__r_e_a_d_m_e.html#autotoc_md6", [
        [ "Suggestions for a good README", "md__r_e_a_d_m_e.html#autotoc_md7", null ],
        [ "Name", "md__r_e_a_d_m_e.html#autotoc_md8", null ],
        [ "Description", "md__r_e_a_d_m_e.html#autotoc_md9", null ],
        [ "Badges", "md__r_e_a_d_m_e.html#autotoc_md10", null ],
        [ "Visuals", "md__r_e_a_d_m_e.html#autotoc_md11", null ],
        [ "Installation", "md__r_e_a_d_m_e.html#autotoc_md12", null ],
        [ "Usage", "md__r_e_a_d_m_e.html#autotoc_md13", null ],
        [ "Support", "md__r_e_a_d_m_e.html#autotoc_md14", null ],
        [ "Roadmap", "md__r_e_a_d_m_e.html#autotoc_md15", null ],
        [ "Contributing", "md__r_e_a_d_m_e.html#autotoc_md16", null ],
        [ "Authors and acknowledgment", "md__r_e_a_d_m_e.html#autotoc_md17", null ],
        [ "License", "md__r_e_a_d_m_e.html#autotoc_md18", null ],
        [ "Project status", "md__r_e_a_d_m_e.html#autotoc_md19", null ]
      ] ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';