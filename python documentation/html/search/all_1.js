var searchData=
[
  ['background_5fcolor_0',['background_color',['../classinterface_1_1_g_u_i.html#a4bcd23f8fc88432b4db8c3c5e44cffc3',1,'interface::GUI']]],
  ['backtracking_1',['backtracking',['../namespacebacktracking.html',1,'']]],
  ['backtracking_2',['Backtracking',['../classbacktracking_1_1_backtracking.html',1,'backtracking']]],
  ['backtracking_2epy_3',['backtracking.py',['../backtracking_8py.html',1,'']]],
  ['backtracking_5fboard_4',['backtracking_board',['../classinterface_1_1_g_u_i.html#a3c8eba202a6098b073a898177d3ca43e',1,'interface::GUI']]],
  ['board_5',['board',['../classbacktracking_1_1_backtracking.html#a1b557fcabac32775665c3601fd963ac0',1,'backtracking.Backtracking.board()'],['../classinterface_1_1_g_u_i.html#a2426bcc820254f93ecf1e47ef6748f8f',1,'interface.GUI.board()'],['../classmain_1_1_sudoku.html#a13af20abfa54235e1784915faa35f63f',1,'main.Sudoku.board()']]],
  ['button_6',['Button',['../classinterface_1_1_button.html',1,'interface']]],
  ['button_5ffunctions_7',['button_functions',['../classinterface_1_1_g_u_i.html#a48ed974cf8828886aed80e4934b35374',1,'interface::GUI']]]
];
