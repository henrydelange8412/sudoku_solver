var annotated_dup =
[
    [ "backtracking", "namespacebacktracking.html", [
      [ "Backtracking", "classbacktracking_1_1_backtracking.html", "classbacktracking_1_1_backtracking" ]
    ] ],
    [ "interface", "namespaceinterface.html", [
      [ "Button", "classinterface_1_1_button.html", "classinterface_1_1_button" ],
      [ "GUI", "classinterface_1_1_g_u_i.html", "classinterface_1_1_g_u_i" ]
    ] ],
    [ "main", "namespacemain.html", [
      [ "Sudoku", "classmain_1_1_sudoku.html", "classmain_1_1_sudoku" ]
    ] ],
    [ "sudoku_generator", "namespacesudoku__generator.html", [
      [ "SudokuGenerator", "classsudoku__generator_1_1_sudoku_generator.html", "classsudoku__generator_1_1_sudoku_generator" ]
    ] ]
];